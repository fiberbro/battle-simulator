const Unit = require('./abstract-unit');

class Soldier extends Unit {
	constructor() {
		super();
		this.experience = 0;
	}

	//Getters
	getExperience() {
		return this.experience;
	}

	getAttack() {
		return 0.5 * (1 + this.health/100) * this.__getRandom(30 + this.experience, 100) / 100;
	}

	getDamage() {
		return 0.05 + this.experience / 100;
	}

	//Object state manipluation
	addExperience() {
		if (this.experience < 50) this.experience += 1;
		return this.experience;
	}
}

module.exports = Soldier;
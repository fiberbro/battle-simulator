class Unit {

	//Private functions - should not be called outside the object
  	__getRandom(min, max) {
		min = Math.floor(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min; 
	}

	__getGeoAvg(map) {

		let base = 1;
		let exponent = 1 / map.size;

		for (let item of map.values()) { base *= item.getAttack(); };

		return (Math.pow(base, exponent)).toFixed(2);
	}

	//Constructor
	constructor() {
		this.health = 100;
		this.recharge = this.__getRandom(100, 2000);
	}

	//Getters
	getHealth() {
		return this.health;
	}

	getRecharge() {
		return this.recharge;
	}

	//Implement in child classes
	getAttack() {
		return null;
	}

	getDamage() {
		return null;
	}

	//Object state manipulation
	decreaseHealth(num) {
		if (num >= this.health) { 
			this.health = 0;
			return "This unit is dead";
		}

		this.health -= num;
		return this.health;
	}

}

module.exports = Unit;
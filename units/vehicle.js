const Unit = require('./abstract-unit');
const Soldier = require('./soldier');

class Vehicle extends Unit {

	//Constructor
	constructor() {
		super();
		this.recharge = this.__getRandom(1001,2000);
		this.operators = new Map();

		//Add random number of operators to the vehicle
		for (let n = 0; n <= (this.__getRandom(1,3)-1); n++) {
			this.operators.set(n, new Soldier());
		}
	}

	//Getters
	getOperators() {
		return this.operators;
	}

	getAttack() {
		return 0.5 * (1 + this.health / 100) * this.__getGeoAvg(this.operators);
	}

	getDamage() {
		let experienceSum = 0;

		for (let operator in this.operators.values()) { experienceSum += operator.getExperience() / 100 };

		return (0.1 + experienceSum).toFixed(2);
	}

	// Object state manipulation
	decreaseHealth(num) {
		let vehicleDamage = num * (30/100); //30 percent
		let randomOperatorDamage = num * (50/100); // 50 percent
		let restDamage = num * (20/100); // Rest

		if (this.operators.size == 1) {
			this.operators.get(0).decreaseHealth(randomOperatorDamage);
			vehicleDamage += restDamage;

			if (this.health <= vehicleDamage) return "Vehicle is dead";

			this.health -= vehicleDamage;
			return this.health;
		}

		else {
			let operatorSize = this.operators.size;
			let randomOperator = this.__getRandom(0, operatorSize - 1);
			let equalChunk = restDamage / (operatorSize - 1);

			this.operators.get(randomOperator).decreaseHealth(randomOperatorDamage);

			for (let index of this.operators.keys()) {
				if (index != randomOperator) this.operators.get(index).decreaseHealth(equalChunk);
			}

			this.health -= vehicleDamage;
			return this.health;
		}
	}

}

module.exports = Vehicle;
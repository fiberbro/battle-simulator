const expect = require('chai').expect;
const Vehicle = require('../units/vehicle');

describe('Vehicle', () => {

	const vehicle = new Vehicle();

    let numOfOperators = vehicle.getOperators().size;

    console.log(numOfOperators)

    it('Recharge limit (1000 - 2000)', () => {
        expect(vehicle.getRecharge()).to.be.above(1000).and.to.be.below(2001);
    });

    it('Vehicle operators are between 1 and 3', () => {
        expect(vehicle.getOperators().size).to.be.above(0).and.to.be.below(4);
    });

    it('Attack is not null', () => {
        expect(vehicle.getAttack()).is.not.null.and.is.a('float');
    });

    it('Damage is not null', () => {
        expect(vehicle.getDamage()).is.not.null.and.is.a('float');
    });

    it('Inflicts correct damage to vehicle', () => {
        if (numOfOperators == 1) {
            expect(vehicle.decreaseHealth(100)).to.equal(50);
        } else {
            expect(vehicle.decreaseHealth(100)).to.equal(70);
        }
    });

});
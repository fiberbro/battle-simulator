const expect = require('chai').expect;
const Unit = require('../units/abstract-unit');

describe('Abstract unit', () => {

	const unit = new Unit();

    it('Health', () => {
        expect(unit.getHealth()).to.equal(100);
    });

    it('Recharge', () => {
        expect(unit.getRecharge()).to.be.above(99).and.to.be.below(2001);
    });

    it('Decrease health', () => {
    	let newHealth = unit.decreaseHealth(50);

    	expect(newHealth).to.equal(50);
    });

    it('Kill unit', () => {
    	let killUnit = unit.decreaseHealth(150);
    	expect(killUnit).to.equal('This unit is dead');
    })

});
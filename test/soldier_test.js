const expect = require('chai').expect;
const Soldier = require('../units/soldier');

describe('Soldier', () => {

	const soldier = new Soldier();

    it('Experience', () => {
        expect(soldier.getExperience()).to.equal(0);
    });

    it('Experience increment', () => {
        expect(soldier.addExperience()).to.equal(1);
    });

    it('Attack is not null', () => {
        expect(soldier.getAttack()).is.not.null.and.is.a('float');
    });

    it('getDamage is not null', () => {
        expect(soldier.getDamage()).is.not.null.and.is.a('float');
    });

});